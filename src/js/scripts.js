//
// Scripts
//

// Ativa o swiper
var swiper_relatorio = new Swiper('.swiper-relatorio', {
    slidesPerView: 'auto',
    spaceBetween: 2,
    freeMode: true,
    loop: true,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
        renderBullet: function (index, className) {
            return '<span class="' + className + '">' + (index + 1) + '</span>';
        }
    }
});

var swiper_imprensa = new Swiper('.swiper-imprensa', {
    slidesPerView: 1,
    freeMode: false,
    loop: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    }
});


window.addEventListener('DOMContentLoaded', event => {

    // Navbar shrink function
    var navbarShrink = function () {
        const navbarCollapsible = document.body.querySelector('#mainNav');
        if (!navbarCollapsible) {
            return;
        }
        if (window.scrollY === 0) {
            navbarCollapsible.classList.remove('navbar-shrink')
        } else {
            navbarCollapsible.classList.add('navbar-shrink')
        }

    };

    // Shrink the navbar 
    navbarShrink();

    // Shrink the navbar when page is scrolled
    document.addEventListener('scroll', navbarShrink);

    // Activate Bootstrap scrollspy on the main nav element
    const mainNav = document.body.querySelector('#mainNav');
    if (mainNav) {
        new bootstrap.ScrollSpy(document.body, {
            target: '#mainNav',
            offset: 74,
        });
    };

    // Collapse responsive navbar when toggler is visible
    const navbarToggler = document.body.querySelector('.navbar-toggler');
    const responsiveNavItems = [].slice.call(
        document.querySelectorAll('#navbarResponsive .nav-link')
    );
    responsiveNavItems.map(function (responsiveNavItem) {
        responsiveNavItem.addEventListener('click', () => {
            if (window.getComputedStyle(navbarToggler).display !== 'none') {
                navbarToggler.click();
            }
        });
    });

    function incluiNoticias(data) {
        function card(noticia) {
            return([
                `<div class="col">`,
                `<div class="card mt-3">`,
                `<h5 class="card-header text-white bg-primary">`,
                `${noticia.meio}`,
                `</h5>`,                
                `<div class="card-body">`,
                `<h5 class="card-title">`,
                `${noticia.titulo}`,
                `</h5>`,
                `<p class="card-text">`,
                `${noticia.descricao}`,
                `</p>`,
                `<a class="btn btn-secondary" href="${noticia.link}" target="_blank" title="${noticia.titulo}" aria-label="${noticia.titulo} (abre em uma nova aba)"><i class="bi bi-chevron-right"></i>Leia</a>`,
                `</div>`,
                `</div>`,
                `</div>`
            ].join("\n"));
        }

        // Add to DOM
        $('.grid-noticias').append(
            data.map(noticia => {
                return(card(noticia));
            }).join("\n")
        );
    }


    const minhasNoticias = $.ajax({
        url : "assets/noticias.json",
        dataType: "json"}).done(incluiNoticias);

});
