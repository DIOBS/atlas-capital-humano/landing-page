'use strict';
const fs = require('fs');
const packageJSON = require('../package.json');
const upath = require('upath');
const sh = require('shelljs');
const UglifyJS = require("uglify-js");

module.exports = function renderScripts() {

    const sourcePath = upath.resolve(upath.dirname(__filename), '../src/js/');
    const destPath = upath.resolve(upath.dirname(__filename), '../dist/js/');

    if (!fs.existsSync(destPath)){
        fs.mkdirSync(destPath);
    }

    fs.readdir(sourcePath, function (err, files) {
        // Handling error
        if (err) {
            return console.log('Unable to scan directory: ' + err);
        }
                
        files.forEach(function (file) {
            const inputFilePath = sourcePath + '/' + file;
            const outputFilePath = destPath + '/' + file.replace('js', 'min.js');
            console.log(outputFilePath);

            const str = fs.readFileSync(inputFilePath).toString();
            const res = UglifyJS.minify(str);

            fs.writeFile(outputFilePath, res.code, function (err) {
                if (err) return console.log(err);
            });
        });
    });
};
