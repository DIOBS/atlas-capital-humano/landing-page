# Atlas do Capital Humano

[![Atlas Capital Humano](screenshot.png)](https://observatoriodefortaleza.fortaleza.ce.gov.br/dados/atlas_capital_humano/)

## Utilização

Clone o repositório e navegue até o diretório raiz do tema. Execute `npm install` e depois `npm start`, que irá abrir um *preview* da página no seu navegador e observar por modificações nos arquivos. Você pode ver o arquivo `package.json` para checar quais scripts estão incluídos.

### Scripts npm

- `npm run build` constrói o projeto - assets, HTML, JS, e CSS no diretório `dist`
- `npm run build:assets` copia os arquivos do diretório `src/assets/` para o diretório `dist`
- `npm run build:pug` compila os arquivos Pug localizados no diretório `src/pug/` para o diretório `dist`
- `npm run build:scripts` copia o arquivo `src/js/scripts.js` para `dist`
- `npm run build:scss` compila os arquivos SCSS em `src/scss/` para o diretório `dist`
- `npm run clean` remove o diretório `dist`, em preparação para reconstruir o projeto
- `npm run start:debug` executa o projeto em modo de depuração
- `npm start` or `npm run start` executa o projeto, inicializa uma visualização em tempo real no seu navegador e observa/aguarda por modificações feitas nos arquivos localizados em `src`

Você precisa ter o [npm](https://www.npmjs.com/) instalado para usar o ambiente de desenvolvimento.

## Sobre

Esta página foi elaborada a partir do Start Bootstrap, que é uma biblioteca de código aberto de temas e templates do Bootstrap. Todos os modelos são disponibilizados com a licença MIT, o que significa que você pode usá-la para qualquer propósito, até mesmo projetos comerciais.

- <https://startbootstrap.com>
- <https://twitter.com/SBootstrap>
- <https://startbootstrap.com/theme/creative>

Start Bootstrap é criado e mantido por **[David Miller](https://davidmiller.io/)**.

- <https://davidmiller.io>
- <https://twitter.com/davidmillerhere>
- <https://github.com/davidtmiller>

Start Bootstrap é baseado na biblioteca [Bootstrap](https://getbootstrap.com/), criada por [Mark Otto](https://twitter.com/mdo) e [Jacob Thorton](https://twitter.com/fat).

## Copyright and License

Copyright 2013-2022 Start Bootstrap LLC. Code released under the [MIT](https://github.com/StartBootstrap/startbootstrap-creative/blob/master/LICENSE) license.
